package com.rafael.utils;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.log4j.Logger;

import com.rafael.gui.model.ClienteBean;
import com.rafael.persistence.entity.Cliente;
import com.rafael.persistence.entity.Cuenta;
import com.rafael.persistence.entity.Movimiento;
/**
 * Clase utilitarios para los beans del sistema
 * 
 * @author Rafael torres
 *
 */
public class Utilitarios {
	
	protected static final Logger lClase = Logger.getLogger(ClienteBean.class);
	
	public static void ordenaClientesListById(List<Cliente> clientesList){
		lClase.debug("+ ordenaClientesListById()");
		
		Collections.sort(clientesList, new Comparator<Cliente>() {
			@Override
			public int compare(Cliente o1, Cliente o2) {
				return o1.getClienteId().compareTo(o2.getClienteId());
			}
		});
		
		lClase.debug("- ordenaClientesListById()");
	}
	
	public static void ordenaClientesListByNombre(List<Cliente> clientesList){
		lClase.debug("+ ordenaClientesListByNombre()");
		Collections.sort(clientesList, new Comparator<Cliente>() {
			@Override
			public int compare(Cliente o1, Cliente o2) {
				return o1.getNombre().toUpperCase().compareTo(o2.getNombre().toUpperCase());
			}
		});
		lClase.debug("- ordenaClientesListByNombre()");
	}
	
	public static void ordenaClientesListByTelefono(List<Cliente> clientesList){
		lClase.debug("+ ordenaClientesListByNombre()");
		Collections.sort(clientesList, new Comparator<Cliente>() {
			@Override
			public int compare(Cliente o1, Cliente o2) {
				return o1.getTelefono().compareTo(o2.getTelefono());
			}
		});
		lClase.debug("- ordenaClientesListByNombre()");
	}
	
	public static void ordenaCuentasListById(List<Cuenta> cuentasList){
		Collections.sort(cuentasList, new Comparator<Cuenta>() {
			@Override
			public int compare(Cuenta o1, Cuenta o2) {
				return o1.getCuentaId().compareTo(o2.getCuentaId());
			}

		});
	}
	
	public static void ordenaCuentasListBySaldo(List<Cuenta> cuentasList) {
		lClase.debug("+ ordenaCuentasListBySaldo()");
		Collections.sort(cuentasList, new Comparator<Cuenta>() {
			@Override
			public int compare(Cuenta o1, Cuenta o2) {
				return o1.getSaldo().compareTo(o2.getSaldo());
			}
		});
		lClase.debug("- ordenaCuentasListBySaldo()");
	}
	
	
	public static void ordenaMovimientosById(List<Movimiento> movimientosList){
		lClase.debug("+ ordenaMovimientosById()");
		Collections.sort(movimientosList, new Comparator<Movimiento>() {
			@Override
			public int compare(Movimiento o1, Movimiento o2) {
				return o1.getMovimientoId().compareTo(o2.getMovimientoId());
			}
		});
		lClase.debug("- ordenaMovimientosById()");
		
	}
	
	public static void ordenaMovimientosByMonto(List<Movimiento> movimientosList){
		lClase.debug("+ ordenaMovimientosByMonto()");
		Collections.sort(movimientosList, new Comparator<Movimiento>() {
			@Override
			public int compare(Movimiento o1, Movimiento o2) {
				return o1.getMonto().compareTo(o2.getMonto());
			}
		});
		lClase.debug("- ordenaMovimientosByMonto()");
	}
	
	public static void ordenaMovimientosByFecha(List<Movimiento> movimientosList){
		lClase.debug("+ ordenaMovimientosByFecha()");
		Collections.sort(movimientosList, new Comparator<Movimiento>() {
			@Override
			public int compare(Movimiento o1, Movimiento o2) {
				return o1.getFecha().compareTo(o2.getFecha());
			}
		});
		lClase.debug("- ordenaMovimientosByFecha()");
	}
	
	public static void ordenaMovimientosByCliente(List<Movimiento> movimientosList){
		lClase.debug("+ ordenaMovimientosByCliente()");
		Collections.sort(movimientosList, new Comparator<Movimiento>() {
			@Override
			public int compare(Movimiento o1, Movimiento o2) {
				return o1.getCliente().getClienteId().compareTo(o2.getCliente().getClienteId());
			}
		});
		lClase.debug("- ordenaMovimientosByCliente()");
	}
	
	public static void ordenaMovimientosByCuenta(List<Movimiento> movimientosList){
		lClase.debug("+ ordenaMovimientosByCuenta()");
		Collections.sort(movimientosList, new Comparator<Movimiento>() {
			@Override
			public int compare(Movimiento o1, Movimiento o2) {
				return o1.getCuenta().getCuentaId().compareTo(o2.getCuenta().getCuentaId());
			}
		});
		lClase.debug("- ordenaMovimientosByCuenta()");
	}

}
