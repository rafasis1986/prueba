package com.rafael.persistence.dao;

import com.rafael.persistence.entity.Cliente;

public interface IClienteDao extends IGenericDao<Cliente, Long> {

}
