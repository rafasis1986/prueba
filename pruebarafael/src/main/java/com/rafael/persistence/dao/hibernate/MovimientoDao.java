package com.rafael.persistence.dao.hibernate;

import org.springframework.stereotype.Repository;

import com.rafael.persistence.dao.IMovimientoDao;
import com.rafael.persistence.entity.Movimiento;

@Repository
public class MovimientoDao extends GenericDao<Movimiento, Long> implements
		IMovimientoDao {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7840262676053936896L;

	
}
