package com.rafael.persistence.dao;

import com.rafael.persistence.entity.Cuenta;

public interface ICuentaDao extends IGenericDao<Cuenta, Long> {

}
