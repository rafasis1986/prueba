package com.rafael.persistence.dao;

import com.rafael.persistence.entity.Movimiento;

public interface IMovimientoDao extends IGenericDao<Movimiento, Long> {

}
