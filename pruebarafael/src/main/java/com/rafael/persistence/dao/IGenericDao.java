package com.rafael.persistence.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.springframework.dao.DataAccessException;

import com.rafael.exception.AccesoDatosException;


public interface IGenericDao<T, ID extends Serializable>  {

	T findById(ID id, boolean lock);

	T getById(ID id, boolean lock);
	
	T getById(ID id ) throws AccesoDatosException;

	public List<T> findAll();

	public List<T> findByExample(T exampleInstance, String... excludeProperty);

	T makePersistent(T entity);

	void makeTransient(T entity);

	void update(Object o) throws DataAccessException;

	public List<T> find(Object[] cList);

}
