package com.rafael.persistence.entity;

import java.io.Serializable;

import javax.persistence.Entity;

@Entity
public class Cliente implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7750563174138043063L;

	private Long clienteId;

	private String nombre;

	private String direccion;

	private String telefono;
	
	public boolean equals(Object o) {
		Cliente cliente=  (Cliente) o;
		boolean retorno = true;
		if (o == null || getClass() != o.getClass()) 
			retorno = false;
		else
			if (clienteId != null ? !clienteId.equals(cliente.clienteId) : cliente.clienteId != null)
				if (nombre != null ? !nombre.equalsIgnoreCase(cliente.nombre) : cliente.nombre != null)				
					return false;
		
		return retorno;
	}

	public Long getClienteId() {
		return clienteId;
	}

	public void setClienteId(Long clienteId) {
		this.clienteId = clienteId;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

}
