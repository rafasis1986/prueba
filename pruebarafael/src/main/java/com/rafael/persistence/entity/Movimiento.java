package com.rafael.persistence.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;


public class Movimiento implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3491870506981690466L;


	private Long movimientoId;

	private BigDecimal monto;

	private Date fecha;

	/**
	 * true para deposito
	 * false para retiro
	 */
	private Boolean tipo;

	private Cuenta cuenta;

	private Cliente cliente;
	
	
	public Long getMovimientoId() {
		return movimientoId;
	}

	public void setMovimientoId(Long movimientoId) {
		this.movimientoId = movimientoId;
	}

	public BigDecimal getMonto() {
		return monto;
	}

	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Boolean getTipo() {
		return tipo;
	}

	public void setTipo(Boolean tipo) {
		this.tipo = tipo;
	}

	public Cuenta getCuenta() {
		return cuenta;
	}

	public void setCuenta(Cuenta cuenta) {
		this.cuenta = cuenta;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

}
