package com.rafael.persistence.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class Cuenta implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8001629347077094005L;

	@NotEmpty(message="indica el codigo de la cuenta")
	private Long cuentaId;
	
	
	private BigDecimal saldo;

	public boolean equals(Object o) {
		Cuenta cta=  (Cuenta) o;
		boolean retorno = true;
		if (o == null || getClass() != o.getClass()) 
			retorno = false;
		else
			if (cuentaId != null ? !cuentaId.equals(cta.cuentaId) : cta.cuentaId != null) 
				retorno = false;
		return retorno;
	}
	
	public Long getCuentaId() {
		return cuentaId;
	}

	public void setCuentaId(Long cuentaId) {
		this.cuentaId = cuentaId;
	}

	public BigDecimal getSaldo() {
		return saldo;
	}

	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}
	
	

}
