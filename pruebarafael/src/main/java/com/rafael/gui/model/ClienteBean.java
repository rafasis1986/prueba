package com.rafael.gui.model;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import com.rafael.exception.AccesoDatosException;
import com.rafael.exception.OperativaException;
import com.rafael.exception.TecnicaException;
import com.rafael.persistence.entity.Cliente;
import com.rafael.service.businesslogic.IClienteService;
import com.rafael.utils.Utilitarios;

@Component
@Scope("request")
@ManagedBean
@RequestScoped
public class ClienteBean extends BaseBean{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -76878566104204298L;
	
	protected static final Logger lClase = Logger.getLogger(ClienteBean.class);

	private Cliente cliente;
	
	private List<Cliente> clientesList;
	
	@Autowired
	private IClienteService clienteService;
	
	private int currentIndex;
	
	private SessionBean sessionBean;
	
	private Long idUltimoClienteCreado;
	
	private boolean activaPopup;
	
	public ClienteBean() {
		cliente = new Cliente();
		setActivaPopup(false);
	}
	
	public void guardarCliente(){
		lClase.debug("+ guardarCliente()");
		sessionBean.setActivaPanelErrorCliente(false);
		try {
			cliente = clienteService.persist(cliente);
			setIdUltimoClienteCreado(cliente.getClienteId());
			lClase.debug("ultimo cliente guardado "+ getIdUltimoClienteCreado());
		} catch (AccesoDatosException e) {
			lClase.error("ocurrio una excepcion guardando un cliente");
			sessionBean.setActivaPanelErrorCliente(true);
			sessionBean.setMsjPanelError("error numero "+ e.getCodigoTroubleshooting());
			sessionBean.setToolTipError("ocurrio una excepcion guardando el cliente"	+ cliente.getClienteId());
		}finally{
			cliente = new Cliente();
			clientesList = null;
		}		
		lClase.debug("- guardarCliente()");
	}
	
	public void borrarCliente(){
		lClase.debug("+ borrarCliente()");
		sessionBean.setActivaPanelErrorCliente(false);
		try {
			if(sessionBean.getCurrentIndex() > -1 ){
				cliente = clienteService.buscaClientePorId(sessionBean.getCurrentIndex());
				clienteService.remove(cliente);
				lClase.debug("se elimino el cliente con el codigo "+cliente.getClienteId());
			}
		} catch (TecnicaException e) {
			lClase.error("problemas removiento el cliente "+sessionBean.getCurrentIndex());
			sessionBean.setActivaPanelErrorCliente(true);
			sessionBean.setMsjPanelError("error numero "+e.getCodigoTroubleshooting());
			sessionBean.setToolTipError(e.getMessage());
		} catch (OperativaException e) {
			lClase.error("Excepcion tecnica numero"+e.getCodigoTroubleshooting());
			lClase.error("verifica estado del cliente: "+ sessionBean.getCurrentIndex());
			sessionBean.setActivaPanelErrorCliente(true);
			sessionBean.setMsjPanelError("error numero" + e.getCodigoTroubleshooting());
			sessionBean.setToolTipError(e.getMessage());
		}catch(DataIntegrityViolationException e){
				lClase.error("Excepcion "+e.getClass());
				lClase.error("verifica movimientos la cuenta: "+ sessionBean.getCurrentIndex());
				sessionBean.setActivaPanelErrorCliente(true);
				sessionBean.setMsjPanelError("excepcion de integridad");
				sessionBean.setToolTipError("Asegurate de eliminar todos los movimientos asociados a la cuenta "+sessionBean.getCurrentIndex());
		}catch (Exception e) {
			lClase.error("Excepcion tecnica numero "+e.getClass());
			lClase.error("verifica estado de la cuenta: "+ sessionBean.getCurrentIndex());
			sessionBean.setActivaPanelErrorCliente(true);
			sessionBean.setMsjPanelError("excepcion no manejada");
		}finally{
			sessionBean.setCurrentIndex(-1);
			clientesList = null;
			cliente = new Cliente();
		}
		
		lClase.debug("- borrarCliente()");
	}
	
	public void actualizarCliente(){
		lClase.debug("+ actualizarCliente");
		sessionBean.setActivaPanelErrorCliente(false);
		try {
			if(sessionBean.getCurrentIndex() > -1){
				cliente.setClienteId(sessionBean.getCurrentIndex());
				clienteService.reload(cliente);
				lClase.debug("se actualizo el cliente Nro: "+cliente.getClienteId());
			}
		}  catch (TecnicaException e) {
			lClase.error("excepcion actualizando el cliente numero "
					+ sessionBean.getCurrentIndex());
			lClase.error(e.getMessage());
			sessionBean.setActivaPanelErrorCliente(true);
			sessionBean.setMsjPanelError("error numero "+e.getCodigoTroubleshooting());
			sessionBean.setToolTipError(e.getMessage());
		} catch (Exception e) {
			lClase.error("excepcion no manejada");
			sessionBean.setActivaPanelErrorCliente(true);
			sessionBean.setMsjPanelError("Ocurrio un error desconocido en el sistema");
			sessionBean.setToolTipError("Por favor consulte al administrador");
		}finally{
			sessionBean.setCurrentIndex(-1);
			cliente = new Cliente();
			clientesList = null;
		}
		lClase.debug("+ actualizarCliente");
	}
	
	
	public void  ordenaClientesListById(){
		lClase.debug("+ ordenaClientesListById()");
		Utilitarios.ordenaClientesListById(getClientesList());
		lClase.debug("- ordenaClientesListById()");
	}   
	

	public void  ordenaClientesListByNombre(){
		lClase.debug("+ ordenaClientesListByNombre()");
		Utilitarios.ordenaClientesListByNombre(getClientesList());
		lClase.debug("- ordenaClientesListByNombre()");
	}
	                                           
	public void  ordenaClientesListByTelefono(){
		lClase.debug("+ ordenaClientesListByTelefono()");
		Utilitarios.ordenaClientesListByTelefono(getClientesList());
		lClase.debug("- ordenaClientesListByTelefono()");
	}
	
	public List<Cliente> getClientesList(){
		if(clientesList==null){
			clientesList = clienteService.getClientes();
		}
		return clientesList;
	}
	
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public int getCurrentIndex() {
		return currentIndex;
	}

	public void setCurrentIndex(int i) {
		this.currentIndex = i;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public Long getIdUltimoClienteCreado() {
		return idUltimoClienteCreado;
	}

	public void setIdUltimoClienteCreado(Long idUltimoClienteCreado) {
		this.idUltimoClienteCreado = idUltimoClienteCreado;
	}

	public boolean isActivaPopup() {
		return activaPopup;
	}

	public void setActivaPopup(boolean activaPopup) {
		this.activaPopup = activaPopup;
	}
	
}
