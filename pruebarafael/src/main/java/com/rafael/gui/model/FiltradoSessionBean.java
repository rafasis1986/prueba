package com.rafael.gui.model;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.rafael.persistence.entity.Movimiento;

@Component
@Scope("session")
public class FiltradoSessionBean extends BaseBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7107948047860205995L;
	
	private Date fechaIni;
	private Date fechaFin;
	private Long cuentaId;
	private List<Movimiento> movimientosList;
	
	
	public Date getFechaIni() {
		return fechaIni;
	}
	public void setFechaIni(Date fechaIni) {
		this.fechaIni = fechaIni;
	}
	public Date getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	public Long getCuentaId() {
		return cuentaId;
	}
	public void setCuentaId(Long cuentaId) {
		this.cuentaId = cuentaId;
	}

}
