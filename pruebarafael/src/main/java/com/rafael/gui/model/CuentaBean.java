package com.rafael.gui.model;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import com.rafael.exception.AccesoDatosException;
import com.rafael.exception.OperativaException;
import com.rafael.exception.TecnicaException;
import com.rafael.persistence.entity.Cuenta;
import com.rafael.service.businesslogic.ICuentaService;
import com.rafael.utils.Utilitarios;

@Component
@Scope("request")
@ManagedBean
@RequestScoped
public class CuentaBean extends BaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3890432354631991149L;

	protected static final Logger lClase = Logger.getLogger(CuentaBean.class);

	private Cuenta cuenta;

	private List<Cuenta> cuentasList;

	@Autowired
	private ICuentaService cuentaService;

	private int currentIndex;

	private SessionBean sessionBean;

	private Long idUltimaCuentaCreada;

	private boolean activaPopup;

	public CuentaBean() {
		cuenta = new Cuenta();
		setActivaPopup(false);
	}

	public void guardarCuenta() {
		lClase.debug("+ guardarCuenta()");
		sessionBean.setActivaPanelErrorCuenta(false);
		try {
			cuenta = cuentaService.persist(cuenta);
			setIdUltimaCuentaCreada(cuenta.getCuentaId());
			lClase.debug("ultima cuenta guardada " + cuenta.getCuentaId());
		} catch (AccesoDatosException e) {
			lClase.error("ocurrio una excepcion guardando la cuenta");
			sessionBean.setActivaPanelErrorCuenta(true);
			sessionBean.setMsjPanelError("error numero "
					+ e.getCodigoTroubleshooting());
			sessionBean
					.setToolTipError("ocurrio una excepcion guardando la cuenta"
							+ cuenta.getCuentaId());
		}
		finally{
			cuenta = new Cuenta();
			cuentasList = null;
			
		}
		lClase.debug("- guardarCuenta()");
	}

	public void borrarCuenta() {
		lClase.debug("+ borrarcuenta()");
		sessionBean.setActivaPanelErrorCuenta(false);
		try {
			if (sessionBean.getCurrentIndex() > -1) {
				cuenta = cuentaService.buscaCuentaPorId(sessionBean.getCurrentIndex());
				cuentaService.remove(cuenta);
			}
		} catch (TecnicaException e) {
			lClase.error("Excepcion tecnica numero"+e.getCodigoTroubleshooting());
			lClase.error("verificar cuenta con id: "+ sessionBean.getCurrentIndex());
			sessionBean.setActivaPanelErrorCuenta(true);
			sessionBean.setMsjPanelError("error numero" + e.getCodigoTroubleshooting());
			sessionBean.setToolTipError("No fue posible borrar la cuenta"+ sessionBean.getCurrentIndex());
		} catch (OperativaException e) {
			lClase.error("Excepcion tecnica numero"+e.getCodigoTroubleshooting());
			lClase.error("verifica estado de la cuenta: "+ sessionBean.getCurrentIndex());
			sessionBean.setActivaPanelErrorCuenta(true);
			sessionBean.setMsjPanelError("error numero" + e.getCodigoTroubleshooting());
			sessionBean.setToolTipError(e.getMessage());
		}catch(DataIntegrityViolationException e){
			lClase.error("Excepcion "+e.getClass());
			lClase.error("verifica movimientos la cuenta: "+ sessionBean.getCurrentIndex());
			sessionBean.setActivaPanelErrorCuenta(true);
			sessionBean.setMsjPanelError("excepcion de integridad");
			sessionBean.setToolTipError("Asegurate de eliminar los movimientos asociados a la cuenta "+sessionBean.getCurrentIndex());
		} catch (Exception e) {
			lClase.error("Excepcion tecnica numero "+e.getClass());
			lClase.error("verifica estado de la cuenta: "+ sessionBean.getCurrentIndex());
			sessionBean.setActivaPanelErrorCuenta(true);
			sessionBean.setMsjPanelError("excepcion no manejada");
		}finally{
			sessionBean.setCurrentIndex(-1);
			cuenta = new Cuenta();
			cuentasList = null;
		}
		lClase.debug("- borrarCuenta()");
	}

	public void actualizarCuenta() {
		lClase.debug("+ actualizarCuenta()");
		sessionBean.setActivaPanelErrorCuenta(false);
		try {
			if(sessionBean.getCurrentIndex()>-1){
				cuenta.setCuentaId(sessionBean.getCurrentIndex());
				cuentaService.reload(cuenta);
			}
		}  catch (TecnicaException e) {
			lClase.error("excepcion actualizando la cuenta numero "
					+ sessionBean.getCurrentIndex());
			lClase.error(e.getMessage());
			sessionBean.setActivaPanelErrorCuenta(true);
			sessionBean.setMsjPanelError("error numero "+e.getCodigoTroubleshooting());
			sessionBean.setToolTipError(e.getMessage());
		} catch (Exception e) {
			lClase.error("excepcion no manejada");
			sessionBean.setActivaPanelErrorCuenta(true);
			sessionBean.setMsjPanelError("Ocurrio un error desconocido en el sistema");
			sessionBean.setToolTipError("Por favor consulte al administrador");
		}finally{
			sessionBean.setCurrentIndex(-1);
			cuenta = new Cuenta();
			cuentasList = null;
		}
		
		lClase.debug("- actualizarCuenta()");
	}

	// funcion que ordena Lista de cuentas ascendentemente por cuentasId
	public void ordenaCuentasListById() {
		lClase.debug("+ ordenaCuentasListById()");
		sessionBean.setActivaPanelErrorCuenta(false);
		Utilitarios.ordenaCuentasListById(getCuentasList());
		lClase.debug("- ordenaCuentasListById()");
	}

	// funcion que ordena Lista de cuentas ascendentemente por saldo
	public void ordenaCuentasListBySaldo() {
		lClase.debug("+ ordenaCuentasListBySaldo()");
		sessionBean.setActivaPanelErrorCuenta(false);
		Utilitarios.ordenaCuentasListBySaldo(cuentaService.getCuentas());
		lClase.debug("- ordenaCuentasListBySaldo()");
	}

	public Cuenta getCuenta() {
		return cuenta;
	}

	public void setCuenta(Cuenta cuenta) {
		this.cuenta = cuenta;
	}

	public List<Cuenta> getCuentasList() {
		if (cuentasList == null)
			cuentasList = cuentaService.getCuentas();
		return cuentasList;
	}

	public void setCuentasList(List<Cuenta> cuentasList) {
		this.cuentasList = cuentasList;
	}

	public ICuentaService getCuentaService() {
		return cuentaService;
	}

	public void setCuentaService(ICuentaService cuentaService) {
		this.cuentaService = cuentaService;
	}

	public int getCurrentIndex() {
		return currentIndex;
	}

	public void setCurrentIndex(int currentIndex) {
		this.currentIndex = currentIndex;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public Long getIdUltimaCuentaCreada() {
		return idUltimaCuentaCreada;
	}

	public void setIdUltimaCuentaCreada(Long idUltimaCuentaCreada) {
		this.idUltimaCuentaCreada = idUltimaCuentaCreada;
	}

	public boolean isActivaPopup() {
		return activaPopup;
	}

	public void setActivaPopup(boolean activaPopup) {
		this.activaPopup = activaPopup;
	}

}
