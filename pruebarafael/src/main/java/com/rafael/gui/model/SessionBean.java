package com.rafael.gui.model;

import java.io.Serializable;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;



@Component
@Scope("session")
public class SessionBean extends BaseBean implements Serializable {

	private static final long serialVersionUID = -2477891600149910534L;
	private static final String PANEL_DEFAULT = "panelDefault";
	
	
	// valor true para activar panel de agragar cuenta
	// false para observar y editar las cuentas en una tabla
	private String currentPanel;
	
	// variable que servira para identificar los elementos seleccionados
	// en las tablas, hace falta que este en la session, porque los bean request 
	// perderian dicha referencia con los eventos a4j
	private long currentIndex;
	
	// almacena la cadena que detalla el error ocurrido
	private String toolTipError;
	
	// guarda una pequeña informacion de las excepciones generadas por el sistema
	private String msjPanelError;
	
	// variable que permite habilitar el anuncio de errores en los panel para clientes
	private boolean activaPanelErrorCliente;
	
	//variable que permite habilitar el anuncio de errores en los panel para movmientos
	private boolean activaPanelErrorMovimiento;
	
	//variable que permite habilitar el anuncio de errores en los panel para cuentas
	private boolean activaPanelErrorCuenta;
	
	//variable que habilita el filtro de los movimientos por Id cliente y fecha
	private boolean activaFiltroMovimiento;
	
	public SessionBean() { 
		currentPanel = PANEL_DEFAULT;
		currentIndex = -1;
		setActivaPanelErrorCliente(false);
		setActivaPanelErrorCuenta(false);
		setActivaPanelErrorMovimiento(false);
		setMsjPanelError("");
		setToolTipError("");
		
	}
	
	public void muestraPanelCuenta(){
		currentPanel = "panelCuenta";
	}
	
	public void muestraPanelCliente(){
		currentPanel = "panelCliente";
	}
	
	public void muestraPanelMovimiento(){
		currentPanel = "panelMovimiento";
	}
	
	public void muestraPanelMovimientoListado(){
		currentPanel = "panelMovimientoListado";
	}
	
	public void muestraPanelManual(){
		currentPanel = PANEL_DEFAULT;
	}

	public String getCurrentPanel() {
		return currentPanel;
	}


	public void setCurrentPanel(String currentPanel) {
		this.currentPanel = currentPanel;
	}

	public long getCurrentIndex() {
		return currentIndex;
	}

	public void setCurrentIndex(long currentIndex) {
		this.currentIndex = currentIndex;
	}

	public String getToolTipError() {
		return toolTipError;
	}

	public void setToolTipError(String toolTipError) {
		this.toolTipError = toolTipError;
	}

	public String getMsjPanelError() {
		return msjPanelError;
	}

	public void setMsjPanelError(String msjPanelError) {
		this.msjPanelError = msjPanelError;
	}

	public boolean isActivaPanelErrorCliente() {
		return activaPanelErrorCliente;
	}

	public void setActivaPanelErrorCliente(boolean activaPanelErrorCliente) {
		this.activaPanelErrorCliente = activaPanelErrorCliente;
	}

	public boolean isActivaPanelErrorMovimiento() {
		return activaPanelErrorMovimiento;
	}

	public void setActivaPanelErrorMovimiento(boolean activaPanelErrorMovimiento) {
		this.activaPanelErrorMovimiento = activaPanelErrorMovimiento;
	}

	public boolean isActivaPanelErrorCuenta() {
		return activaPanelErrorCuenta;
	}

	public void setActivaPanelErrorCuenta(boolean activaPanelErrorCuenta) {
		this.activaPanelErrorCuenta = activaPanelErrorCuenta;
	}

	public boolean isActivaFiltroMovimiento() {
		return activaFiltroMovimiento;
	}

	public void setActivaFiltroMovimiento(boolean activaFiltroMovimiento) {
		this.activaFiltroMovimiento = activaFiltroMovimiento;
	}

}