package com.rafael.gui.model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.rafael.exception.OperativaException;
import com.rafael.exception.TecnicaException;
import com.rafael.persistence.entity.Cliente;
import com.rafael.persistence.entity.Cuenta;
import com.rafael.persistence.entity.Movimiento;
import com.rafael.service.businesslogic.IClienteService;
import com.rafael.service.businesslogic.ICuentaService;
import com.rafael.service.businesslogic.IMovimimentoService;
import com.rafael.utils.Utilitarios;

@Component
@Scope("request")
@ManagedBean
@RequestScoped
public class MovimientoBean extends BaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1308718852754012686L;
	
	protected Logger lClase = Logger.getLogger(MovimientoBean.class);

	private Movimiento movimiento;

	private List<Movimiento> movimientosList;

	@Autowired
	private IMovimimentoService movimientoService;

	@Autowired
	private IClienteService clienteService;

	@Autowired
	private ICuentaService cuentaService;

	private int currentIndex;

	private Long IdUltimoMovimientoCreado;

	private SessionBean sessionBean;

	List<SelectItem> listClientes;

	List<SelectItem> listCuentas;

	private Long clienteId;

	private Long cuentaId;

	private boolean activaPopup;
	
	private Date fechaIni;
	
	private Date fechaFin;
	
	public MovimientoBean() {
		movimiento = new Movimiento();
		setActivaPopup(false);
	}

	public void procesarMovimiento() {
		lClase.debug("+ procesarMovimientos()");
		Cuenta cta = null;
		Cliente cli = null;
		sessionBean.setActivaPanelErrorMovimiento(false);
		try {
			cta = cuentaService.buscaCuentaPorId(Long.valueOf(cuentaId));
			cli = clienteService.buscaClientePorId(Long.valueOf(clienteId));
			movimiento.setCliente(cli);
			movimiento.setCuenta(cta);
			movimiento.setFecha(new Date(System.currentTimeMillis()));
			movimientoService.ejecutaTransaccion(movimiento);
			movimiento = movimientoService.persist(movimiento);
			setIdUltimoMovimientoCreado(movimiento.getMovimientoId());
		} catch (TecnicaException e) {
			lClase.error("procesando el movimiento "+movimiento.getMovimientoId());
			lClase.error("realizado por el cliente " +getClienteId()+", sobre la cuenta "+getCuentaId());
			sessionBean.setActivaPanelErrorMovimiento(true);
			sessionBean.setMsjPanelError("error numero: "+e.getCodigoTroubleshooting());
			sessionBean.setToolTipError(e.getMessage());
		} catch (OperativaException e) {
			lClase.error("excepcion operativa procesando el movimiento "+movimiento.getMovimientoId());
			lClase.error("realizado por el cliente " +getClienteId()+", sobre la cuenta "+getCuentaId());
			sessionBean.setActivaPanelErrorMovimiento(true);
			sessionBean.setMsjPanelError("error numero: "+e.getCodigoTroubleshooting());
			sessionBean.setToolTipError(e.getMessage());
		}finally{
			movimiento = new Movimiento();
			movimientosList = null;
		}
		lClase.debug("- procesarMovimientos()");
	}

	public void borrarMovimiento() {
		lClase.debug("+ borrarMovimiento()");
		sessionBean.setActivaPanelErrorMovimiento(false);
		try {
			if (sessionBean.getCurrentIndex() > -1) {
				movimiento = movimientoService.buscaMovimientoPorId(sessionBean.getCurrentIndex());
				if(movimiento.getTipo()){
					movimiento.setTipo(false);
				}
				else{
					movimiento.setTipo(true);
				}
				movimientoService.ejecutaTransaccion(movimiento);
				movimientoService.remove(movimiento);
				lClase.debug("se ha removido el movimiento nro: "+movimiento.getMovimientoId());
				lClase.debug("Que afectaba las cuenta "+movimiento.getCuenta().getCuentaId());
				lClase.debug("Realizado por el cliente "+movimiento.getCliente().getClienteId());
			}
		} catch (TecnicaException e) {
			lClase.error("excepcion tecnica numero "+e.getCodigoTroubleshooting());
			lClase.error("movimiento nro: "+movimiento.getMovimientoId());
			sessionBean.setActivaPanelErrorMovimiento(true);
			sessionBean.setMsjPanelError("error numero "+e.getCodigoTroubleshooting()+" borrando el mov. "+sessionBean.getCurrentIndex());
			sessionBean.setToolTipError(e.getMessage());
		} catch (OperativaException e) {
			lClase.error("excepcion operativa numero "+e.getCodigoTroubleshooting()+" borrando el mov. "+sessionBean.getCurrentIndex());
			lClase.error("movimiento nro: "+movimiento.getMovimientoId());
			sessionBean.setActivaPanelErrorMovimiento(true);
			sessionBean.setMsjPanelError("error numero "+e.getCodigoTroubleshooting());
			sessionBean.setToolTipError(e.getMessage());
		}finally{
			sessionBean.setCurrentIndex(-1);
			movimiento = new Movimiento();
			movimientosList = null;
		}
		lClase.debug("- borrarMovimiento()");
	}
	
	
	public void editarMovimiento(){
		lClase.debug("+ editarMovimiento()");
		sessionBean.setActivaPanelErrorMovimiento(false);
		Movimiento movimientoAux = null;
		try{
			if (sessionBean.getCurrentIndex() > -1) {
				movimientoAux = movimientoService.buscaMovimientoPorId(sessionBean.getCurrentIndex());
				movimiento.setCuenta(cuentaService.buscaCuentaPorId(getCuentaId()));
				movimiento.setCliente(clienteService.buscaClientePorId(getClienteId()));
				movimiento.setMovimientoId(movimientoAux.getMovimientoId());
				movimiento.setTipo(movimientoAux.getTipo());
				movimiento.setFecha(new Date(System.currentTimeMillis()));
				movimiento = movimientoService.actualizaMovimiento(movimientoAux, movimiento);
				lClase.debug("se edito el movimiento nro: "+movimiento.getMovimientoId());
			}
		} catch (TecnicaException e) {
			lClase.error("excepcion tecnica numero "+e.getCodigoTroubleshooting());
			lClase.error("movimiento nro: "+movimiento.getMovimientoId());
			sessionBean.setActivaPanelErrorMovimiento(true);
			sessionBean.setMsjPanelError("error numero "+e.getCodigoTroubleshooting()+" editando el mov. "+sessionBean.getCurrentIndex());
			sessionBean.setToolTipError(e.getMessage());
		}catch (OperativaException e) {
			lClase.error("excepcion operativa numero "+e.getCodigoTroubleshooting());
			lClase.error("movimiento nro: "+sessionBean.getCurrentIndex());
			sessionBean.setActivaPanelErrorMovimiento(true);
			sessionBean.setMsjPanelError("error numero "+e.getCodigoTroubleshooting()+" editando el mov. "+sessionBean.getCurrentIndex());
			sessionBean.setToolTipError(e.getMessage());
		}catch (Exception e) {
			lClase.error("excepcion no manejada "+e.toString());
			lClase.error("movimiento nro: "+sessionBean.getCurrentIndex());
			sessionBean.setActivaPanelErrorMovimiento(true);
			sessionBean.setToolTipError("no se pudo editar el movimiento nro "+sessionBean.getCurrentIndex());
			sessionBean.setMsjPanelError("error no manejado, consulte al administrador del sistema");
		}finally{
			sessionBean.setCurrentIndex(-1);
			movimiento = new Movimiento();
			movimientosList = null;
		}
		lClase.debug("- editarMovimiento()");
	}
	
	public void ordenaMovimientosById() {
		lClase.debug("+ ordenaCuentasListById()");
		sessionBean.setActivaPanelErrorMovimiento(false);
		Utilitarios.ordenaMovimientosById(getMovimientosList());
		lClase.debug("- ordenaCuentasListById()");
	}
	
	public void ordenaMovimientosByMonto() {
		lClase.debug("+ ordenaCuentasListByMonto()");
		sessionBean.setActivaPanelErrorMovimiento(false);
		Utilitarios.ordenaMovimientosByMonto(getMovimientosList());
		lClase.debug("- ordenaCuentasListByMonto()");
	}
	
	public void ordenaMovimientosByFecha() {
		lClase.debug("+ ordenaCuentasListByFecha()");
		sessionBean.setActivaPanelErrorMovimiento(false);
		Utilitarios.ordenaMovimientosByFecha(getMovimientosList());
		lClase.debug("- ordenaCuentasListByFecha()");
	}
	
	public void ordenaMovimientosByCuenta() {
		lClase.debug("+ ordenaCuentasListByCuenta()");
		sessionBean.setActivaPanelErrorMovimiento(false);
		Utilitarios.ordenaMovimientosByCuenta(getMovimientosList());
		lClase.debug("- ordenaCuentasListByCuenta()");
	}
	
	public void ordenaMovimientosByCliente() {
		lClase.debug("+ ordenaCuentasListByCliente()");
		sessionBean.setActivaPanelErrorMovimiento(false);
		Utilitarios.ordenaMovimientosByCliente(getMovimientosList());
		lClase.debug("- ordenaCuentasListByCliente()");
	}

	public List<Movimiento> getMovimientosList() {
		if (movimientosList == null) {
			movimientosList = movimientoService.getMovimientos();
		}
		return movimientosList;
	}

	public void setMovimientosList(List<Movimiento> movimientosList) {
		this.movimientosList = movimientosList;
	}

	public Movimiento getMovimiento() {
		return movimiento;
	}

	public void setMovimiento(Movimiento movimiento) {
		this.movimiento = movimiento;
	}

	public int getCurrentIndex() {
		return currentIndex;
	}

	public void setCurrentIndex(int currentIndex) {
		this.currentIndex = currentIndex;
	}

	public Long getIdUltimoMovimientoCreado() {
		return IdUltimoMovimientoCreado;
	}

	public void setIdUltimoMovimientoCreado(Long idUltimoMovimientoCreado) {
		IdUltimoMovimientoCreado = idUltimoMovimientoCreado;
	}

	public List<SelectItem> getListClientes() {
		List<String> listAux = null;
		SelectItem selectItem = null;
		if (listClientes == null) {
			listClientes = new ArrayList<SelectItem>();
			listAux = clienteService.listaClientesId();
			for (String item : listAux) {
				selectItem = new SelectItem();
				selectItem.setLabel(item);
				selectItem.setValue(item);
				listClientes.add(selectItem);
			}
		}
		return listClientes;
	}

	public void setListClientes(List<SelectItem> listClientes) {
		this.listClientes = listClientes;
	}

	public List<SelectItem> getListCuentas() {
		List<String> listAux = null;
		SelectItem selectItem = null;
		if (listCuentas == null) {
			listCuentas = new ArrayList<SelectItem>();
			listAux = cuentaService.listaCuentasId();
			for (String item : listAux) {
				selectItem = new SelectItem();
				selectItem.setLabel(item);
				selectItem.setValue(item);
				listCuentas.add(selectItem);
			}
		}
		return listCuentas;
	}

	public void setListCuentas(List<SelectItem> listCuentas) {
		this.listCuentas = listCuentas;
	}

	public Long getClienteId() {
		return clienteId;
	}

	public void setClienteId(Long clienteId) {
		this.clienteId = clienteId;
	}

	public Long getCuentaId() {
		return cuentaId;
	}

	public void setCuentaId(Long cuentaId) {
		this.cuentaId = cuentaId;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public boolean isActivaPopup() {
		return activaPopup;
	}

	public void setActivaPopup(boolean activaPopup) {
		this.activaPopup = activaPopup;
	}


}
