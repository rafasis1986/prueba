package com.rafael.gui.model;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.rafael.exception.AccesoDatosException;
import com.rafael.persistence.entity.Movimiento;
import com.rafael.service.businesslogic.IClienteService;
import com.rafael.service.businesslogic.ICuentaService;
import com.rafael.service.businesslogic.IMovimimentoService;

@Component
@Scope("request")
@ManagedBean
@RequestScoped
public class MovimientoFiltradoBean extends BaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1308718852754012686L;
	
	protected Logger lClase = Logger.getLogger(MovimientoFiltradoBean.class);

	private Movimiento movimiento;

	private List<Movimiento> movimientosList;

	@Autowired
	private IMovimimentoService movimientoService;

	@Autowired
	private IClienteService clienteService;
	
	@Autowired
	private ICuentaService cuentaService;

	private SessionBean sessionBean;

	List<SelectItem> listClientes;

	private Long clienteId;
	
	private Date fechaIni;
	
	private Date fechaFin;
	
	public MovimientoFiltradoBean() {
		
	}
	
	public List<Movimiento> getMovimientosList() {
		lClase.debug("+ getMovimientosList");
		java.sql.Date ini = null;
		java.sql.Date fin = null;
		try {
			if (sessionBean.isActivaFiltroMovimiento()
					&& sessionBean.getCurrentIndex() > -1) {
				if (fechaIni != null && fechaFin != null) {
					ini = new java.sql.Date(fechaIni.getTime());
					fin = new java.sql.Date(fechaFin.getTime());
					setClienteId(sessionBean.getCurrentIndex());
					if (fin.after(ini)) {
						movimientosList = movimientoService.consultarPorClienteFecha(clienteService
										.buscaClientePorId(getClienteId()),	ini, fin);
					}
				}
			}
		} catch (AccesoDatosException e) {
			lClase.error("ha ocurrido el error "+e.getCodigoTroubleshooting());
			lClase.error("error consultando los movimientos asociads al cliente "+sessionBean.getCurrentIndex());
			sessionBean.setActivaPanelErrorMovimiento(true);
			sessionBean.setMsjPanelError(e.getMessage());
		}catch (Exception e) {
			lClase.error("excepcion no manejada "+e.toString());
			lClase.error("error consultando los movimientos asociads al cliente "+sessionBean.getCurrentIndex());
			sessionBean.setActivaPanelErrorMovimiento(true);
			sessionBean.setToolTipError("error listando los movimientos del cliente "+sessionBean.getCurrentIndex());
			sessionBean.setMsjPanelError("ha ocurrido un comportamirnto irregular contacte al administrador del sistema");

		}
		
		lClase.debug("+ getMovimientosList");
		return movimientosList;
	}
	
	public void aplicarFiltro(){
		sessionBean.setActivaFiltroMovimiento(true);
	}

	public void setMovimientosList(List<Movimiento> movimientosList) {
		this.movimientosList = movimientosList;
	}

	public Movimiento getMovimiento() {
		return movimiento;
	}

	public void setMovimiento(Movimiento movimiento) {
		this.movimiento = movimiento;
	}
	
	public List<SelectItem> getListClientes() {
		List<String> listAux = null;
		SelectItem selectItem = null;
		if (listClientes == null) {
			listClientes = new ArrayList<SelectItem>();
			listAux = clienteService.listaClientesId();
			for (String item : listAux) {
				selectItem = new SelectItem();
				selectItem.setLabel(item);
				selectItem.setValue(item);
				listClientes.add(selectItem);
			}
		}
		return listClientes;
	}

	public void setListClientes(List<SelectItem> listClientes) {
		this.listClientes = listClientes;
	}

	
	public Long getClienteId() {
		return clienteId;
	}

	public void setClienteId(Long clienteId) {
		this.clienteId = clienteId;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public Date getFechaIni() {
		return fechaIni;
	}


	public void setFechaIni(Date fechaIni) {
		
		this.fechaIni = fechaIni;
	}


	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

}
