package com.rafael.service.businesslogic;

import java.util.Date;
import java.util.List;

import com.rafael.exception.AccesoDatosException;
import com.rafael.exception.CuentaNoExisteException;
import com.rafael.exception.SaldoInsuficienteException;
import com.rafael.persistence.entity.Cliente;
import com.rafael.persistence.entity.Movimiento;

public interface IMovimimentoService {
	
	public Movimiento persist(Movimiento movimiento);
	
	public List<Movimiento> getMovimientos();
	
	public void remove(Movimiento movimiento);
	
	public void reload(Movimiento movimiento);
	
	public void ejecutaTransaccion(Movimiento movimiento) throws CuentaNoExisteException, AccesoDatosException, SaldoInsuficienteException;
	
	public Movimiento buscaMovimientoPorId(Long movimientoId) throws AccesoDatosException;
	
	public Movimiento actualizaMovimiento(Movimiento movAnterior, Movimiento movNuevo) throws SaldoInsuficienteException, CuentaNoExisteException, AccesoDatosException;

	public List<Movimiento> consultarPorClienteFecha(Cliente cliente, Date initDate,
			Date endDate) throws AccesoDatosException;

}
