package com.rafael.service.businesslogic.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rafael.exception.AccesoDatosException;
import com.rafael.exception.RestriccionException;
import com.rafael.persistence.dao.ICuentaDao;
import com.rafael.persistence.entity.Cuenta;
import com.rafael.service.businesslogic.ICuentaService;

@Service
public class CuentaService implements ICuentaService {

	private ICuentaDao cuentaDao;

	@Autowired
	public void setCuentaDao(ICuentaDao cuentaDao) {
		this.cuentaDao = cuentaDao;
	}

	@Transactional
	public Cuenta persist(Cuenta cuenta) throws AccesoDatosException {
		return cuentaDao.makePersistent(cuenta);
		
	}

	@Transactional(readOnly = true)
	public List<Cuenta> getCuentas() {
		List<Cuenta> list = cuentaDao.findAll();
		return list;
	}

	@Transactional
	public void remove(Cuenta cuenta) throws RestriccionException {
		try{
			cuentaDao.makeTransient(cuenta);
		}catch(Exception e){
			throw new RestriccionException("verifica que no tenga movimientos asociados", e);
		}
		

	}

	@Transactional
	public void reload(Cuenta cuenta) throws AccesoDatosException {
		try{
			cuentaDao.update(cuenta);
		}catch(DataAccessException e){
			throw new AccesoDatosException("ha ocurrido un error actualizando la cuenta", e);
		}
	}

	@Transactional(readOnly = true)
	public List<String> listaCuentasId() {
		List<Cuenta> list = cuentaDao.findAll();
		List<String> listaResultado = new ArrayList<String>();
		for (Cuenta o : list) {
			listaResultado.add(String.valueOf(o.getCuentaId()));
		}
		return listaResultado;
	}

	@Transactional(readOnly=true)
	public Cuenta buscaCuentaPorId(Long cuentaId) throws AccesoDatosException {
		return cuentaDao.getById(cuentaId);
	}

}
