package com.rafael.service.businesslogic;

import java.util.List;

import com.rafael.exception.AccesoDatosException;
import com.rafael.exception.RestriccionException;
import com.rafael.persistence.entity.Cliente;

public interface IClienteService {
	
	public Cliente persist(Cliente cliente) throws AccesoDatosException;
	
	public List<Cliente> getClientes();
	
	public void remove(Cliente cliente) throws RestriccionException;
	
	public void reload(Cliente cliente) throws AccesoDatosException;
	
	public List<String> listaClientesId();
	
	public Cliente buscaClientePorId(Long clienteId) throws AccesoDatosException;

}
