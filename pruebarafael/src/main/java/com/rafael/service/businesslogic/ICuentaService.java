package com.rafael.service.businesslogic;

import java.util.List;

import com.rafael.exception.AccesoDatosException;
import com.rafael.exception.RestriccionException;
import com.rafael.persistence.entity.Cuenta;

public interface ICuentaService{
	
	public Cuenta persist(Cuenta cuenta) throws AccesoDatosException;
	
	public List<Cuenta> getCuentas();
	
	public void remove(Cuenta cuenta) throws RestriccionException;
	
	public void reload(Cuenta cuenta) throws AccesoDatosException;
	
	public List<String> listaCuentasId();
	
	public Cuenta buscaCuentaPorId(Long cuentaId) throws AccesoDatosException;

}
