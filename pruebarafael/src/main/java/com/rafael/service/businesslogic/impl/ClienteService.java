package com.rafael.service.businesslogic.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rafael.exception.AccesoDatosException;
import com.rafael.exception.RestriccionException;
import com.rafael.persistence.dao.IClienteDao;
import com.rafael.persistence.entity.Cliente;
import com.rafael.service.businesslogic.IClienteService;

@Service
public class ClienteService implements IClienteService {

	IClienteDao clienteDao;
	
	@Autowired
	public void setClienteDao(IClienteDao clienteDao) {
		this.clienteDao = clienteDao;
	}

	@Transactional
	public Cliente persist(Cliente cliente) throws AccesoDatosException{
		return clienteDao.makePersistent(cliente);
	}

	@Transactional(readOnly=true)
	public List<Cliente> getClientes() {
		List<Cliente> list = clienteDao.findAll();
		return list;
	}

	@Transactional
	public void remove(Cliente cliente) throws RestriccionException {
		try{
			clienteDao.makeTransient(cliente);
		}catch (HibernateException e){
			throw new RestriccionException("verifique que ell cliente no tenga movimientos asociados", e);
		}
		
	}

	@Transactional
	public void reload(Cliente cliente) throws AccesoDatosException {
		try{
		clienteDao.update(cliente);
		}catch(DataAccessException e){
			throw new AccesoDatosException("ha ocurrido un error actualizando los datos del cliente "+cliente.getClienteId(), e);
		}
	}
	
	@Transactional(readOnly=true)
	public List<String> listaClientesId(){
		List<Cliente> list = clienteDao.findAll();
		List<String> listaResultado = new ArrayList<String>();
		for(Cliente o : list){
			listaResultado.add(String.valueOf(o.getClienteId()));
		}
		return listaResultado;
	}

	@Transactional(readOnly=true)
	public Cliente buscaClientePorId(Long clienteId) throws AccesoDatosException {
		return clienteDao.getById(clienteId);
	}
	

}
