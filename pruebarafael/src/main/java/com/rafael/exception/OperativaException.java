package com.rafael.exception;

/**
 * Excepcion que representa un error de los procesos de negocio
 * 
 * @author Rafael Torres
 */
public abstract class OperativaException extends AplicacionException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -540606857531488700L;

	/**
	 * Codigo alfanumérico para el troubleshooting
	 */
	protected String codigoTroubleshooting;

	/**
	 * String descriptivo del error para los usuarios
	 */
	protected String descripcionNegocioPorDefecto;

	/**
	 * Constructor por defecto
	 * 
	 * @param causa
	 *            Instancia que contiene la excepción padre capturada para
	 *            lanzar esta excepción.
	 */

	public OperativaException(Throwable causa) {
		super(causa);
	}

	/**
	 * Inicializa la excepcion con una causa y un mensaje de troubleshooting
	 * 
	 * @param message
	 *            Mensaje para el troubleshooting
	 * @param causa
	 */

	public OperativaException(String message, Throwable causa) {
		super(message, causa);
		this.descripcionNegocioPorDefecto = message != null ? message : this.descripcionNegocioPorDefecto;
	}

	/**
	 * @return the codigoTroubleshooting
	 */
	public String getCodigoTroubleshooting() {
		return codigoTroubleshooting;
	}


	/**
	 * Getter para el campo {@link #descripcionNegocioPorDefecto}
	 * 
	 * @return El valor actual del campo <strong>{@link #descripcionNegocioPorDefecto}
	 *         </strong>
	 */
	public String getDescripcionNegocioPorDefecto() {
		return descripcionNegocioPorDefecto;
	}

	/* (non-Javadoc)
	 * @see java.lang.Throwable#toString()
	 */
	public String toString() {
		return String.format("[O%s-%s] Error operativo: '%s'. Causa: %s", codigoTroubleshooting, getCodigoHash(), descripcionNegocioPorDefecto, getCause());
	}

}
