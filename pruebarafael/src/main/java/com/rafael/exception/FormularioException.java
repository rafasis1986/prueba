package com.rafael.exception;

/**
 * Excepcion que captura las faltas de datos ocurridas en los formularios
 * 
 * @author Rafael Torres
 *
 */
public class FormularioException extends OperativaException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5878501267610489358L;
	
	
	public FormularioException(String message, Throwable causa) {
		super(message, causa);
		this.codigoTroubleshooting = "03";
		this.descripcionNegocioPorDefecto = message;
	}

}
