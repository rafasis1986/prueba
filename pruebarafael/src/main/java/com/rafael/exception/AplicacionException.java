package com.rafael.exception;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

/**
 * 
 * <p>
 * Excepcion base de la jerarquia de Troubleshooting.
 * <p>
 * Representa una excepcion dentro de la aplicacion Prueba la cual puede ser
 * <em>tecnica</em> u <em>operativa</em>.
 * 
 * @author Rafael Torres
 * 
 */
public class AplicacionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7715764494232505945L;

	/**
	 * String con un codigo hash unico para cada instancia de la excepcion
	 */
	private String codigoHash;

	public AplicacionException(String message) {
		super(message);
		codigoHash = generarClave();

	}

	/**
	 * Constructor por defecto
	 * 
	 * @param causa
	 * 	          Instancia que contiene la excepcion padre capturada para
	 *            lanzar esta excepcion. 
	 */
	public AplicacionException(Throwable causa) {
		super(causa);
		codigoHash = generarClave();

	}

	/**
	 * Inicializa la excepcion con una causa y un mensaje de troubleshooting
	 * 
	 * @param message
	 *            Mensaje para el troubleshooting
	 * @param causa
	 */
	public AplicacionException(String message, Throwable causa) {
		super(message, causa);
		codigoHash = generarClave();
	}

	/**
	 * Genera el codigo hash de la excepcion, a traves de un algoritmo de Digest
	 * 
	 * @return {@link String} con el valor hash para la instancia actual
	 */
	protected String generarClave() {
		long ahora = System.currentTimeMillis();
		Random random = new Random(ahora);
		String randomNumber = random.nextLong() + "";
		String message = randomNumber + this.hashCode();
		MessageDigest md;
		String result;
		try {
			md = MessageDigest.getInstance("MD5");
			md.reset();
			byte messageDigest[] = md.digest(message.getBytes());

			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < messageDigest.length; i++) {
				hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
			}
			result = hexString.toString();

		} catch (NoSuchAlgorithmException e) {
			result = randomNumber;

		}
		return result;
	}

	public String getCodigoHash() {
		return codigoHash;
	}

}
