package com.rafael.exception;

/**
 * Excepcion utilizada para los casos en donde las cuentas a debitadar
 * poseen menos saldo que el monto del movimiento
 * 
 * @author Rafael Torres
 */
public class SaldoInsuficienteException extends OperativaException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1543670259246476728L;
	
	/**
	 * Constructor por defecto
	 * 
	 * @param message
	 * 				Texto descriptivo de las causas de la excepcion
	 * @param causa
	 * 				Instancia que contiene la excepcion padre
	 */
	public SaldoInsuficienteException(String message, Throwable causa) {
		super(message, causa);
		this.codigoTroubleshooting = "01";
		this.descripcionNegocioPorDefecto = message;
	}


}
