package com.rafael.exception;

/**
 * Excepción que representa una falla técnica dentro de la aplicación
 * 
 * Toda excepción técnica debe extender esta clase.
 * 
 * @author Rafael Torres
 */
public abstract class TecnicaException extends AplicacionException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9083051725162232784L;

	/**
	 * Código alfanumérico de troubleshooting
	 */
	protected String codigoTroubleshooting;

	/**
	 * Descripción técnica para usuarios
	 */
	protected String descripcionTecnica;

	/**
	 * Inicializa la excepción con mensaje de troubleshooting
	 * 
	 * @param message
	 *            Mensaje para el troubleshooting
	 * */
	public TecnicaException(String message) {
		super(message);
	}

	/**
	 * Constructor por defecto
	 * 
	 * @param causa
	 *            Instancia que contiene la excepción padre capturada para
	 *            lanzar esta excepción. El valor puede ser <tt>null</tt>
	 */
	public TecnicaException(Throwable causa) {
		super(causa);
	}

	/**
	 * Inicializa la excepción con una causa y un mensaje de troubleshooting
	 * 
	 * @param message
	 *            Mensaje para el troubleshooting
	 * @param causa
	 */
	public TecnicaException(String message, Throwable causa) {
		super(message, causa);
		this.descripcionTecnica = message != null ? message : this.descripcionTecnica;
	}

	/**
	 * Getter para el campo {@link #codigoTroubleshooting}
	 * 
	 * @return El valor actual del campo <strong>{@link #codigoTroubleshooting}
	 *         </strong>
	 */
	public String getCodigoTroubleshooting() {
		return codigoTroubleshooting;
	}

	/**
	 * Setter para el campo {@link #codigoTroubleshooting}
	 * 
	 * @param codigoTroubleshooting
	 *            el nuevo valor a establecer para el campo <strong>
	 *            {@link #codigoTroubleshooting}</strong>
	 */
	public void setCodigoTroubleshooting(String codigoTroubleshooting) {
		this.codigoTroubleshooting = codigoTroubleshooting;
	}

	/**
	 * Getter para el campo {@link #descripcionTecnica}
	 * 
	 * @return El valor actual del campo <strong>{@link #descripcionTecnica}
	 *         </strong>
	 */
	public String getDescripcionTecnica() {
		return descripcionTecnica;
	}

	/**
	 * Setter para el campo {@link #descripcionTecnica}
	 * 
	 * @param descripcionTecnica
	 *            el nuevo valor a establecer para el campo <strong>
	 *            {@link #descripcionTecnica}</strong>
	 */
	public void setDescripcionTecnica(String descripcionTecnica) {
		this.descripcionTecnica = descripcionTecnica;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Throwable#toString()
	 */
	public String toString() {
		return String.format("[T%s-%s] Error técnico: '%s'. Causa: %s", codigoTroubleshooting, getCodigoHash(), descripcionTecnica, getCause());
	}
}
