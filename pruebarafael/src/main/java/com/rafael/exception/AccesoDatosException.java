/**
 * 
 */
package com.rafael.exception;


/**
 * 
 * Excepción que se dispara cuando se produce un error en el acceso a fuentes de datos.
 * 
 * @author Rafael Torres
 */
public class AccesoDatosException extends TecnicaException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 587629440166233758L;

	
	 /**
	 * Inicializa la excepción con una causa y un mensaje de troubleshooting
	 * 
	 * @param message
	 *            Mensaje para el troubleshooting
	 * @param causa
	 *            Instancia que contiene la excepción padre capturada para
	 *            lanzar esta excepción.
	 */
	 public AccesoDatosException(String message, Throwable causa) {
		super(message, causa);

		this.codigoTroubleshooting = "51";
	}
}

