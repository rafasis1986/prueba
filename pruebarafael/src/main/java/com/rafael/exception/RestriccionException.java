package com.rafael.exception;

/**
 * Excepcion para manejar los errores de dependencia al momento de 
 * eliminar un registro
 * 
 * @author Rafael Torres
 *
 */
public class RestriccionException extends OperativaException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2398705848354185701L;
	
	
	public RestriccionException(String message, Throwable causa) {
		super(message, causa);
		this.codigoTroubleshooting = "04";
		this.descripcionNegocioPorDefecto = message;
	}

}
