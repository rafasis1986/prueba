package com.rafael.exception;

/**
 *  Excepcion utilizada para los casos en donde las cuentas a debitadar
 * 	poseen menos saldo que el monto del movimiento
 * 
 * @author Rafael Torres
 *
 */
public class CuentaNoExisteException extends OperativaException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1070530130754224691L;

	public CuentaNoExisteException(String message, Throwable causa) {
		super(message, causa);
		this.codigoTroubleshooting = "02";
		this.descripcionNegocioPorDefecto = message;
	}

}
